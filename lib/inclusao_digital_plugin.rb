require_relative 'string'

class InclusaoDigitalPlugin < Noosfero::Plugin

  require_relative './inclusao_digital_plugin/custom_fields_filler'
  require_relative './inclusao_digital_plugin/pids_loader'
  require_relative './inclusao_digital_plugin/transform'
  require_relative './inclusao_digital_plugin/better_csv_row'

  def self.plugin_name
    # FIXME
    "InclusaoDigitalPlugin"
  end

  def self.plugin_description
    # FIXME
    _("Plugin for the Rede Brasil Digital portal.")
  end

  def self.fill_custom_fields
    InclusaoDigitalPlugin::CustomFieldsFiller.fill_data
  end

  def self.destroy_custom_fields
    InclusaoDigitalPlugin::CustomFieldsFiller.destroy
  end

  def self.transform
    InclusaoDigitalPlugin::Transform.execute
  end

  def self.load_pids
    InclusaoDigitalPlugin::PidsLoader.load
  end

  def self.csv_first_data_row
    10 
  end

  def self.load_all_from_scratch
    transform
    destroy_custom_fields
    fill_custom_fields
    load_pids
  end

  def self.destroy_communities_and_load
    transform
    destroy_custom_fields
    fill_custom_fields
    Community.destroy_all
    load_pids
  end

  CSV::Converters[:sanitizer] = lambda do |s|
    if s.class == String
      s.strip!
      s.gsub!(/\ {2,}/,' ')
      ActionView::Base.full_sanitizer.sanitize(s)
    end
    s
  end

end
